<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Players_bl {

    public static function create($data) {
        $uri = G_PUBLIC."players/";
        $image = Fox\Utils\Image::upload($uri, $_FILES["player"],$data["name"]);
        $data["pic"] = "players/".$image;
        $player = Player::instanciate($data);
        $r = $player->create();
        return $r;
    }
    
    public static function iHavePlayers(Club $club){
       $players = Player::where("club", $club->getId());
       return $players;
    }

}
