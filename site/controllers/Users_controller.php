<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Users_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        
        $this->view->render($this,"index","View Title");
    }
    
    public function login(){
        $data = filter_input_array(INPUT_POST);
        $r = Users_bl::login($data);
        \Fox\Core\Penelope::printJSON($r);
    }
    
    public function register(){
        $data = filter_input_array(INPUT_POST);
        $r = Users_bl::create($data);
        \Fox\Core\Penelope::printJSON($r);
    }
    
    public function destroySession(){
        Users_bl::logout();
    }
    
    public function encrypt(){
        $r = Fox\Utils\Hash::encrypt($_GET["string"], "formatic");
        print_r($r);
    }
    
    public function decrypt(){
        $r = Fox\Utils\Hash::decrypt($_GET["string"], $_GET["key"]);
        print_r($r);
    }
    
    public function hash(){
        $r = Fox\Utils\Hash::create($_GET["string"]);
        print_r($r);
    }
    
}
