<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Player_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
      $userId = Fox\Core\Session::get("uid");
        $this->view->uid = $userId;
        $club = Users_bl::iHaveAClub($userId);
        $this->view->players = Players_bl::iHavePlayers($club);
        foreach ($this->view->players as $player) {
            $player->populate("from","Position");
            $player->populate("from","Country");
        }
        $this->view->title="Soccer Shop Admin Panel | Crear Jugador";
        $this->view->render($this,"index");
    }
    
    public function crear(){
        $userId = Fox\Core\Session::get("uid");
        $this->view->uid = $userId;
        $this->view->countries = Country::getAll();
        $this->view->positions = Position::getAll();
        $this->view->club = Users_bl::iHaveAClub($userId);
        $this->view->title="Soccer Shop Admin Panel | Crear Jugador";
        $this->view->render($this,"crear");
    }
    
    public function create(){
        $data = filter_input_array(INPUT_POST);
        $r = Players_bl::create($data);
        //$r["error"] = 0;
        if($r["error"] == 0){
            $msg = "Jugador creado exitosamente";
        }else{
            $msg = $r["msg"];
        }
        echo '<script> window.parent.alert("'.$msg.'"); console.log(window.parent["bacano"]());</script>';
    }
    
    public function delete(){
        $id = filter_input(INPUT_POST, "id");
        $player = Player::getById($id);
        $r = $player->delete();
        Fox\Core\Penelope::printJSON($r);
    }
    
    public function update(){
        $id = filter_input(INPUT_POST, "id");
        $data = filter_input_array(INPUT_POST);
        unset($data["id"]);
        $player = Player::getById($id);
        foreach ($data as $key => $attr) {
            $player->{"set".ucfirst($key)}($attr);
        }
        $r = $player->update();
        Fox\Core\Penelope::printJSON($r);
    }

}
