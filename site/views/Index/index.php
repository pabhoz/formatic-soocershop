<?php $this->loadModule("header"); ?>
<body>

    <nav>
      <?php if($this->session): ?>
        <div class="menuBtn" data-menu="user"><?php echo ucfirst($this->user->getName()); ?> &darr;</div> 
      <?php else: ?>
        <div class="menuBtn" data-menu="login">Login</div>
        <div class="menuBtn" data-menu="register">Registro</div>
      <?php endif; ?>
    </nav>

    <div class="niceForm user">
      <div>
        <a href="<?php echo ROOT; ?>admin">Admin Panel</a>
      </div>
      <div onclick="logout()">Logout</div>
    </div>

    <div class="niceForm login">
      <form onsubmit="ajaxSubmit(event)" action="Users/login">
        <input name="name" type="text" placeholder="Username" required>
        <input name="password" type="password" placeholder="Password" required>
        <button>Login</button>
      </form>
    </div>

    <div class="niceForm register">
      <form onsubmit="ajaxSubmit(event)" action="Users/register">
        <input name="id" type="hidden">
        <input name="name" type="text" placeholder="Username" required>
        <input name="email" type="email" placeholder="email" required>
        <input name="password" type="password" placeholder="Password" required>
        <!--input name="password2" type="password" placeholder="verificar password" required-->
        <button>Registrar</button>
      </form>
    </div>

    <div class="parallax-container" data-parallax="scroll" data-position="top" 
         data-bleed="0" data-image-src="<?php echo URL; ?>public/images/bgs/wcup.jpg" 
        data-natural-width="1635" data-natural-height="920" style="height: 562px;">
    </div>

    <section>
      <div class="container">
        About Us
      </div>
    </section>
    
    <div class="parallax-container" data-parallax="scroll" data-position="top" 
         data-bleed="10" data-speed="0.2" data-image-src="<?php echo URL; ?>public/images/bgs/players.jpg" 
        data-natural-width="1400" data-natural-height="900" style="height: 562px;">
    </div>
    
    <section>
      <div class="container">
          <ul>
        <?php foreach ($this->players as $player) : ?>
          <li onclick="addToCar(event)" data-id="<?php print($player->getId()); ?>"><?php print($player->getName()); ?></li>
        <?php endforeach; ?>
          </ul>
      </div>
    </section>
    
    <div class="parallax-container" data-parallax="scroll" data-position="top" 
         data-bleed="0" data-speed="0.3" data-image-src="<?php echo URL; ?>public/images/bgs/clubs.jpg" 
        data-natural-width="1920" data-natural-height="1152" style="height: 562px;">
    </div>
    
    <section>
      <div class="container">
        Clubes
      </div>
    </section>
    
    <footer>
        Footer
    </footer>
    <?php $this->loadModule("scripts"); ?>

    <script>
      function addToCar(e){
          var id = $(e.target).data("id");
          
          $.ajax({
              url: "Players/addToCar",
              method: "POST",
              data: {
                  id: id
              }
          }).done(function(r){
             console.log(r); 
          });
      }
        
      $(".menuBtn").click(function(){
        
        $("."+$(this).data("menu")).slideToggle();

      });
      
      function logout(){
         $.ajax({
          method: "GET",
          url: "<?php echo URL; ?>Users/destroySession"
        }).done( () => {
            location = location;
        });
      }
    </script>

    <?php $this->loadModule("ajaxFormSubmit"); ?>
    
</body>
</hmtl>


