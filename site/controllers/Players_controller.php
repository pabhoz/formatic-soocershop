<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Players_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $players = Player::getAll();
        $this->view->players = $players;        //print_r($players);
        $this->view->render($this,"index","Jugadores");
    }
    
    public function crear(){
        $this->view->render($this,"crear","Crear Jugador");
    }
    
    public function guardar(){
        $player = new Player(null, "Test", "test.png");
        $r = $player->create();
        print_r($r);
    }
    
    public function addToCar(){
        $id = filter_input(INPUT_POST, "id");
        $player = Player::getById($id);
        $uid = Users_bl::getSession();
        $club = Users_bl::iHaveAClub($uid);
        
        $player->belongsToMany("ShoppingCarts", $club);
        print_r($player);
        $r = $player->update();
        Fox\Core\Penelope::printJSON($r);
    }
    
}
