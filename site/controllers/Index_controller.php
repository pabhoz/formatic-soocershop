<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Index_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->session = Fox\Core\Session::get("uid");
        if($this->view->session){
            $this->view->user = User::getById($this->view->session);
        }
        $this->view->players = Player::getAll(false,true);        //print_r($this->view->players);
        $this->view->render($this,"index","View Title");
    }
    
    public function saludar($nombre){
        echo "HOLA $nombre";
    }
    
}
