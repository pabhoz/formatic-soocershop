<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php print(URL); ?>public/dist/img/user2-160x160.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php print(ucfirst($this->user->getName())); ?></p>
        <span><i class="fa fa-user-circle text-red"></i>	&nbsp;CEO</span>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!-- treeview -->
      <li class="active treeview">
        
        <!-- treeview Title -->
        <!-- Boton inicial del grupo -->
        <a href="#">
          <i class="fa fa-heartbeat"></i> <span>Mi Club</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <!-- fin boton principal -->
        <!-- /treeview Title -->
        
        <!-- treeview Content -->
        <ul class="treeview-menu">
          <?php if($this->club == null): ?>
          <!-- treeview content elment -->
          <li class="active">
            <a class="asyncLink" href="<?php print(URL); ?>Club/crear" >
              <i class="fa fa-circle-o"></i> Crear Club
            </a>
          </li>
          <!-- /treeview content elment -->
          <?php else: ?>
          <!-- treeview content elment -->
          <li>
            <a class="asyncLink" href="<?php print(URL); ?>Club/" >
              <i class="fa fa-circle-o"></i> Perfil
            </a>
          </li>
          <!-- /treeview content elment -->
          <?php endif; ?>
        </ul>
        <!-- /treeview Content -->
      </li>
      
      <!-- treeview -->
      <li class="treeview">
        
        <!-- treeview Title -->
        <!-- Boton inicial del grupo -->
        <a href="#">
          <i class="fa fa-users"></i> <span>Jugadores</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <!-- fin boton principal -->
        <!-- /treeview Title -->
        
        <!-- treeview Content -->
        <ul class="treeview-menu">
          <!-- treeview content elment -->
          <li class="active">
            <a class="asyncLink" href="<?php print(URL); ?>Player/crear" >
              <i class="fa fa-circle-o"></i> Crear Jugador
            </a>
          </li>
          <li class="active">
            <a class="asyncLink" href="<?php print(URL); ?>Player/" >
              <i class="fa fa-circle-o"></i> Listar Jugadores
            </a>
          </li>
 
        </ul>
        <!-- /treeview Content -->
      </li>
      <?php /*
      <li class="treeview">
        <!-- treeview Title -->
        <a href="#">
          <i class="fa fa-heartbeat"></i> <span>Sample Openend Group</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <!-- /treeview Title -->
        <!-- treeview Content -->
        <ul class="treeview-menu">
          <!-- treeview content elment -->
          <li class="active">
            <a class="asyncLink" href="<?php print(URL); ?>Example/dummyData" >
              <i class="fa fa-circle-o"></i> Sample Option
            </a>
          </li>
          <!-- /treeview content elment -->
          <!-- treeview content elment -->
          <li>
            <a class="asyncLink" href="<?php print(URL); ?>Example/dummyData" >
              <i class="fa fa-circle-o"></i> Sample Option
            </a>
          </li>
          <!-- /treeview content elment -->
        </ul>
        <!-- /treeview Content -->
      </li>
      <!--/treeview -->
      
      <!-- treeview -->
      <li class="treeview">
        <!-- treeview Title -->
        <a class="asyncLink" href="<?php print(URL); ?>Example/dummyData" >
          <i class="fa fa-user-plus"></i> <span>Single Option</span>
        </a>
        <!-- /treeview Content -->
      </li>
      <!--/treeview -->
      
      <!-- treeview -->
      <li class="treeview">
        <!-- treeview Title -->
        <a href="#">
          <i class="fa fa-hand-grab-o"></i> <span>Closed Sample Group</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>          
        </a>
        <!-- /treeview Content -->
        <ul class="treeview-menu">
          <!-- treeview content elment -->
          <li class="active">
            <a class="asyncLink" href="<?php print(URL); ?>Example/dummyData" >
              <i class="fa fa-circle-o"></i> Sample Option
            </a>
          </li>
          <!-- /treeview content elment -->
          <!-- treeview content elment -->
          <li>
            <a class="asyncLink" href="<?php print(URL); ?>Example/dummyData" >
              <i class="fa fa-circle-o"></i> Sample Option
            </a>
          </li>
          <!-- /treeview content elment -->
        </ul>
      </li>
      <!--/treeview -->
      */ ?>
        </ul>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
