<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Club_controller extends \Fox\FoxController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->club = Users_bl::iHaveAClub(Fox\Core\Session::get("uid"));
        $this->view->title="Soccer Shop Admin Panel | Gestión de club";
        $this->view->render($this,"index");
    }
    
    public function crear()
    {   
        $this->view->uid = Fox\Core\Session::get("uid");
        $this->view->countries = Country::getAll();
        $this->view->title="Soccer Shop Admin Panel | Crear club";
        $this->view->render($this,"crear");
    }
    
    public function create()
    {   
        $data = filter_input_array(INPUT_POST);
        $r = Clubs_bl::create($data);
        \Fox\Core\Penelope::printJSON($r);
    }

}
