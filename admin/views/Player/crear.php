<section class="content-header">
      <h1>
        Gestión del Jugador
        <small>Crear Jugador</small>
      </h1>
    </section>
<section class="content">
<div class="col-md-12">
    <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Jugador</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="Player/create" method="POST" enctype="multipart/form-data" target="lasucia">
              <div class="box-body">
                  <input type="hidden" name="id" value="null">
                <div class="form-group">
                  <label>Nombre</label>
                  <input name="name" type="text" class="form-control" placeholder="Enter ...">
               </div>
               <div class="form-group">
                  <label>País</label>
                  <select name="country" class="form-control" required>
                    <option value="">Seleccione país</option>
                    <?php foreach ($this->countries as $country) : ?>
                        <option value="<?php echo $country["id"]; ?>"><?php echo $country["name"]; ?></option>
                    <?php endforeach; ?>
                </select>
                </div>
                <div class="form-group">
                  <label>Posicion</label>
                  <select name="position" class="form-control" required>
                    <option value="">Seleccione posicion</option>
                    <?php foreach ($this->positions as $position) : ?>
                        <option value="<?php echo $position["id"]; ?>"><?php echo $position["name"]; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="form-group">
                <label>Fecha Nacimiento :</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <!--<input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">-->
                  <input name="born" class="form-control" data-provide="datepicker">
                </div>
                <!-- /.input group -->
              </div>
              <div class="form-group">
              <label>Pierna Habil:</label>
                  <div class="radio">
                    <label>
                      <input type="radio" name="leg" id="optionsRadios1" value="rigth" checked="">
                      Derecho
                    </label>
                    <label>
                      <input type="radio" name="leg" id="optionsRadios2" value="left">
                      Zurdo
                    </label>
                    <label>
                      <input type="radio" name="leg" id="optionsRadios3" value="both">
                      Ambas
                    </label>
                  </div>
                </div>
                <div class="row">
                <div class="form-group col-xs-6 col-sm-3">
                  <label>Velocidad</label>
                  <input name="pac" type="number" min="0"  max="100" class="form-control" placeholder="Enter ...">
               </div>
               <div class="form-group col-xs-6 col-sm-3">
                  <label>Tiro</label>
                  <input name="sho" type="number" min="0"  max="100" class="form-control" placeholder="Enter ...">
               </div>
               <div class="form-group col-xs-6 col-sm-3">
                  <label>Pase</label>
                  <input name="pas" type="number" min="0"  max="100" class="form-control" placeholder="Enter ...">
               </div>
               <div class="form-group col-xs-6 col-sm-3">
                  <label>Regate</label>
                  <input name="dri" type="number" min="0"  max="100" class="form-control" placeholder="Enter ...">
               </div>
               <div class="form-group col-xs-6 col-sm-3">
                  <label>Defensa</label>
                  <input name="def" type="number" min="0"  max="100" class="form-control" placeholder="Enter ...">
               </div>
               <div class="form-group col-xs-6 col-sm-3">
                  <label>Cabeceo</label>
                  <input name="phy" type="number" min="0"  max="100" class="form-control" placeholder="Enter ...">
               </div>
               </div>
               <div class="form-group">
                  <label for="exampleInputFile">Imagen</label>
                  <input type="file" name="player" id="exampleInputFile">

                  <p class="help-block">El escudo debe pesar menos de 2MB</p>
                </div>
                <div class="form-group">
                <label>Precio</label>
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input name="price" type="text" class="form-control">
                </div>
                </div>
                <div class="form-group">
                  <label>Club</label>
                  <input type="hidden" name="club" value="<?php echo $this->club->getId(); ?>">
                  <input type="text" class="form-control" value="<?php echo $this->club->getName(); ?>" disabled>
               </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <iframe style="display:none;" name="lasucia"></iframe>
            </form>
    </div>
</div>
</section>
<script>
  function bacano(){
    <?php $this->reloadThis("Player/crear"); ?>
  }
</script>