<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Users_bl {

    public static function logout() {
        Fox\Core\Session::destroy();
    }

    public static function login($data) {
        
        $data["password"] = Fox\Utils\Hash::create($data["password"]);
        $user = User::getBy("name", $data["name"]);
        if (empty($user)) {
            $r = ["error" => 1, "msg" => "No existe el usuario"];
        } else {
            $r = ($user->getPassword() == $data["password"]) ? ["error" => 0, "msg" => "Bienvenido"] : ["error" => 1, "msg" => "La contraseña no coincide"];
            Users_bl::setSession($user);
        }
        return $r;
    }

    public static function create($data) {
        
        $data["password"] = Fox\Utils\Hash::create($data["password"]);
        $user = User::getBy("name", $data["name"]);
        if (empty($user)) {
            $user = User::instanciate($data);
            $r = $user->create();
            if (empty($r)) {
                $r = ["error" => 1, "msg" => "Error al crear el usuario"];
            }
        } else {
                $r = ["error" => 1, "msg" => "Usuario ya existe"];
        }
        return $r;
    }
    
    private static function setSession(User $user){
        Fox\Core\Session::set("uid", $user->getId());
    }
    
    public static function getSession(){
        return Fox\Core\Session::get("uid");
    }
    
    public static function iHaveAClub($uid){
       $club = Club::getBy("user", $uid);
       return $club;
    }

}
