<section class="content-header">
      <h1>
        Gestión del club
        <small>Crear club</small>
      </h1>
    </section>

<div class="col-md-12">
<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Quick Example</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="Club/create" method="POST" enctype="multipart/form-data" target="lasucia">
              <div class="box-body">
                  <input type="hidden" name="user" value="<?php echo $this->uid; ?>">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nombre</label>
                  <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                </div>
                <div class="form-group">
                  <div class="form-group">
                  <label>País</label>
                  <select name="country" class="form-control" required>
                    <option value="">Seleccione país</option>
                    <?php foreach ($this->countries as $country) : ?>
                        <option value="<?php echo $country["id"]; ?>"><?php echo $country["name"]; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Escudo</label>
                  <input type="file" name="shield" id="exampleInputFile">

                  <p class="help-block">El escudo debe pesar menos de 2MB</p>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
              <iframe name="lasucia"></iframe>
            </form>
          </div>
    
    
   </div>