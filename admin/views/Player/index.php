<div class="box">
  <div class="box-header">
    <h3 class="box-title">Data Table With Full Features</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="players" class="display" style="width:100%">
      <thead>
        <tr>
          <th>Nombre</th>
          <th>País</th>
          <th>Posición</th>
          <th>Nacimiento</th>
          <th>Leg</th>
          <th>Pac</th>
          <th>Sho</th>
          <th>Pas</th>
          <th>Dri</th>
          <th>Def</th>
          <th>Phy</th>
          <th>Precio</th>
          <th>Opciones</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($this->players as $player) : ?>
        <tr id="player<?php print($player->getId());?>">
          <td class="editable" data-type="text" data-name="name"><?php print($player->getName()); ?></td>
          <td><?php print($player->getCountry()[0]->getName()); ?></td>
          <td><?php print($player->getPosition()[0]->getName()); ?></td>
          <td><?php print($player->getBorn()); ?></td>
          <td class="editable" data-type="text" data-name="leg"><?php print($player->getLeg()); ?></td>
          <td><?php print($player->getPac()); ?></td>
          <td class="editable" data-type="number" data-name="sho"><?php print($player->getSho()); ?></td>
          <td><?php print($player->getPas()); ?></td>
          <td><?php print($player->getDri()); ?></td>
          <td><?php print($player->getDef()); ?></td>
          <td><?php print($player->getPhy()); ?></td>
          <td><?php print($player->getPrice()); ?></td>
          <td> <a onclick="deleteElement(event)" data-name="<?php print($player->getName()); ?>" data-id="<?php print($player->getId()); ?>"><i class="fa fa-trash-o"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>

<script>

  function generateInput(el,type){
    var leInput = document.createElement("input");
          leInput.type = type;
          leInput.value = el.html();
          leInput.name = el.data("name");
          leInput.placeholder = leInput.value;
          leInput.onblur = function(){
            if(this.value != ""){
              asyncUpdate(this);
            }
          };
          return leInput;
  }

  $(()=>{
    $(".editable").dblclick(function(){
      let el = $(this);
      switch (el.data("type")) {
        case "text":
          var leInput = generateInput(el,"text");
          el.html("");
          el.append(leInput);
          break;
        case "number":
          var leInput = generateInput(el,"number");
          el.html("");
          el.append(leInput);
          break;
      }
    });
  });

  function asyncUpdate(el){
   var id = $(el.parentNode.parentNode).find("i").parent().data("id");
   var data = {id: id};
   data[`${el.name}`] = `${el.value}`;
   $.ajax({
     url: "Player/update",
     method: "POST",
     data: data
   }).done(function(r){
     let respuesta = JSON.parse(r);
     if(respuesta.error == 0){
       el.parentNode.innerHTML = el.value;
     }
   });
  }
</script>

<script>

    $(function(){

      $('#players').DataTable({
        "order": [[0, "desc"]],
        "lengthMenu": [ 5, 10, 20, 50, 100 ]
      });

    });

    function deleteElement(e){
      let el = $(e.target.parentNode);
      var entonces = confirm(`Desea eliminar de la nomina al jugar ${el.data("name")}?`);
      if(entonces){
        
        $.ajax({
          url: "Player/delete",
          method: "POST",
          data: { id: el.data("id")}
        }).done(function(r){
          response = JSON.parse(r);
          if(response.error == 0){
            var parent = document.querySelector(`#player${el.data("id")}`).parentNode;
            parent.removeChild(document.querySelector(`#player${el.data("id")}`));
          }else{
            alert(response.msg);
          }
        });

      }
    }

</script>